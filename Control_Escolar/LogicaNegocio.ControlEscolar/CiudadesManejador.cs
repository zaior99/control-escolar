﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CiudadesManejador
    {
        private CiudadesAccesoDatos _ciudadesAccesoDatos = new CiudadesAccesoDatos();

        public List<Ciudades> GetCiudades(string filtro)
        {
            var listCiudades = _ciudadesAccesoDatos.GetCiudades(filtro);

            return listCiudades;
        }
    }
}
