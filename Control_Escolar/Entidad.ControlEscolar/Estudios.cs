﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Estudios
    {
        private int _idestudio;
        private string _nombre;
        private string _estudio;       
        private string _documento;

        public int Idestudio { get => _idestudio; set => _idestudio = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Estudio { get => _estudio; set => _estudio = value; }
        public string Documento { get => _documento; set => _documento = value; }
    }
}
