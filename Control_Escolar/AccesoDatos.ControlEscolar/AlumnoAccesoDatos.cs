﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;


namespace AccesoDatos.ControlEscolar
{
    public class AlumnoAccesoDatos
    {
        ConexionAccesoDatos conexion;      

        public AlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);         
        }
        public void Guardar(Alumnos alumno)
        {
            if (alumno.Idalumno == 0)
            {
                string consulta = string.Format("Insert into alumno values(null,'{0}','{1}','{2}','{3}'," +
                "'{4}','{5}','{6}','{7}','{8}','{9}')", alumno.Nocontrol, alumno.Nombre, 
                alumno.Apellidopaterno, alumno.Apellidomaterno, alumno.Fechanacimiento, alumno.Domicilio, 
                alumno.Email, alumno.Sexo, alumno.Estado, alumno.Ciudad);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update alumno set numerocontrol ='{0}', nombre ='{1}', " +
                    "apellidopaterno ='{2}', apellidomaterno ='{3}', fechanacimiento ='{4}'," +
                    " domicilio ='{5}', email ='{6}', sexo ='{7}', estado ='{8}', ciudad ='{9}'" +
                    " where idalumno = {10}", alumno.Nocontrol, alumno.Nombre, alumno.Apellidopaterno,
                    alumno.Apellidomaterno, alumno.Fechanacimiento, alumno.Domicilio, alumno.Email,
                    alumno.Sexo,alumno.Estado,alumno.Ciudad, alumno.Idalumno);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idAlumno)
        {
            string consulta = string.Format("Delete from alumno where idalumno = {0}", idAlumno);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Alumnos> GetAlumno(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listAlumno = new List<Alumnos>();
            var ds = new DataSet();
            string consulta = "Select * from alumno where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "alumno");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumnos
                {
                    Idalumno = Convert.ToInt32(row["idalumno"]),
                    Nocontrol = row["numerocontrol"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanacimiento = row["fechanacimiento"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Email = row["email"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Estado = row["estado"].ToString(),
                    Ciudad =  row["ciudad"].ToString()
                };
                listAlumno.Add(alumno);
            }
            return listAlumno;
        }
    }
}
