﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Escolar
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmControlEscolar usuarios = new frmControlEscolar();
            usuarios.Show();
        }

        private void alumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alumno alumno = new Alumno();
            alumno.Show();
        }

        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profesor profesor = new Profesor();
            profesor.Show();
        }

        private void estudioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Estudio estudio = new Estudio();
            estudio.Show();
        }

        private void materiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RelacionMateria rm = new RelacionMateria();
            rm.Show();
        }

        private void escuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Escuelas escuela = new Escuelas();
            escuela.Show();
        }
    }
}
