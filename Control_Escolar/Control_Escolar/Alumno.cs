﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;


namespace Control_Escolar
{
    public partial class Alumno : Form
    {
        private AlumnoManejador _alumnoManejador;
        private EstadoManejador _estadoManejador;
        private CiudadesManejador _ciudadesManejador;
        
        public Alumno()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadoManejador = new EstadoManejador();
            _ciudadesManejador = new CiudadesManejador();
            
        }

        private void Alumno_Load(object sender, EventArgs e)
        {
            buscaralumno("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscaralumno(string filtro)
        {

            dtgAlumno.DataSource = _alumnoManejador.GetAlumno(filtro);
        }
        private void TraerEstados(string filtro)
        {
            cmbEstado.DataSource = _estadoManejador.GetEstados(filtro);
            cmbEstado.DisplayMember = "codigoestado";

        }
        private void TraerCiudades(string filtro)
        {
            cmbCiudad.DataSource = _ciudadesManejador.GetCiudades(cmbEstado.Text);
            cmbCiudad.DisplayMember = "nombre";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNoControl.Enabled = activar;
            txtNombre.Enabled = activar;
            txtAPaterno.Enabled = activar;
            txtAmaterno.Enabled = activar;
            txtFechanacimiento.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtEmail.Enabled = activar;
            txtSexo.Enabled = activar;
            cmbEstado.Enabled = activar;
            cmbCiudad.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNoControl.Text = "";
            txtNombre.Text = "";
            txtAPaterno.Text = "";
            txtAmaterno.Text = "";
            txtFechanacimiento.Text = "";
            txtDomicilio.Text = "";
            txtEmail.Text = "";
            txtSexo.Text = "";
            cmbEstado.Text = "";
            cmbCiudad.Text = "";
        }
        private void GuardarAlumno()
        {
            _alumnoManejador.Guardar(new Alumnos
            {
                Idalumno = Convert.ToInt32(lblId.Text),
                Nocontrol = txtNoControl.Text,
                Nombre = txtNombre.Text,
                Apellidopaterno = txtAPaterno.Text,
                Apellidomaterno = txtAmaterno.Text,
                Fechanacimiento = txtFechanacimiento.Text,
                Domicilio = txtDomicilio.Text,
                Email = txtEmail.Text,
                Sexo = txtSexo.Text,
                Estado = cmbEstado.Text,
                Ciudad = cmbCiudad.Text

            });
        }
        private void EliminarAlumno()
        {
            var Idalumno = dtgAlumno.CurrentRow.Cells["idalumno"].Value;
            _alumnoManejador.Eliminar(Convert.ToInt32(Idalumno));
        }
        private void ModificarAlumno()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgAlumno.CurrentRow.Cells["idalumno"].Value.ToString();
            txtNoControl.Text = dtgAlumno.CurrentRow.Cells["nocontrol"].Value.ToString();
            txtNombre.Text = dtgAlumno.CurrentRow.Cells["nombre"].Value.ToString();
            txtAPaterno.Text = dtgAlumno.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtAmaterno.Text = dtgAlumno.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            txtFechanacimiento.Text = dtgAlumno.CurrentRow.Cells["fechanacimiento"].Value.ToString();
            txtDomicilio.Text = dtgAlumno.CurrentRow.Cells["domicilio"].Value.ToString();
            txtEmail.Text = dtgAlumno.CurrentRow.Cells["email"].Value.ToString();
            txtSexo.Text = dtgAlumno.CurrentRow.Cells["sexo"].Value.ToString();
            cmbEstado.Text = dtgAlumno.CurrentRow.Cells["estado"].Value.ToString();
            cmbCiudad.Text = dtgAlumno.CurrentRow.Cells["ciudad"].Value.ToString();


        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscaralumno(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNoControl.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarAlumno();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAlumno();
                    buscaralumno("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgAlumno_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumno();
                buscaralumno("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerCiudades("");
        }

        private void cmbEstado_Click(object sender, EventArgs e)
        {
            TraerEstados("");
        }

        private void cmbCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblApaterno_Click(object sender, EventArgs e)
        {

        }

        private void dtgAlumno_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
