﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class ProfesorAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ProfesorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Profesores profesor)
        {
            if (profesor.Idprofesor == 0)
            {
                string consulta = string.Format("Insert into profesor values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')",
                profesor.Nocontrolp, profesor.Nombre,profesor.Apellidopaterno, profesor.Apellidomaterno, profesor.Direccion, profesor.Ciudad,
                profesor.Estado, profesor.Numerocedula, profesor.Titulo,profesor.Fechanacimeinto);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update profesor set nocontrolp ='{0}',nombre='{1}', " +
                "apellidopaterno ='{2}', apellidomaterno ='{3}', direccion ='{4}', ciudad ='{5}', estado ='{6}'" +
                ", numerocedula ='{7}', titulo ='{8}', fechanacimiento ='{9}' where idprofesor = {10}",
                profesor.Nocontrolp, profesor.Nombre, profesor.Apellidopaterno, profesor.Apellidomaterno, profesor.Direccion, profesor.Ciudad,
                profesor.Estado, profesor.Numerocedula, profesor.Titulo, profesor.Fechanacimeinto,profesor.Idprofesor);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idProfesor)
        {
            string consulta = string.Format("Delete from profesor where idprofesor = {0}", idProfesor);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Profesores> GetProfesor(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listProfesor = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "Select * from profesor where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesor");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesor = new Profesores
                {
                    Idprofesor = Convert.ToInt32(row["idprofesor"]),
                    Nocontrolp = row["nocontrolp"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Ciudad = row["ciudad"].ToString(),
                    Estado = row["estado"].ToString(),
                    Numerocedula = row["numerocedula"].ToString(),
                    Titulo = row["titulo"].ToString(),
                    Fechanacimeinto = row["fechanacimiento"].ToString()
                    
                };
                listProfesor.Add(profesor);
            }
            return listProfesor;
        }
        public DataSet Ncontrol(string i)
        {
            var ds = new DataSet();
            string consulta = string.Format("select max(substring(nocontrolp,-2)) as 'nocontrolp' from profesor where nocontrolp like '%" + i + "%'");
            ds = conexion.ObtenerDatos(consulta, "profesor");
            return ds;
        }
    }
}
