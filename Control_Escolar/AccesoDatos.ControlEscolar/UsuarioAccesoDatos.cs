﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class UsuarioAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public UsuarioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Usuario usuario)
        {
            if (usuario.Idusuario == 0)
            {
                string consulta = string.Format("Insert into usuario values(null,'{0}','{1}','{2}','{3}')", 
                usuario.Nombre, usuario.Apellidopaterno, usuario.Apellidomaterno, usuario.Contrasenia);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update usuario set nombre ='{0}',apellidopaterno ='{1}', " +
                "apellidomaterno ='{2}', contrasenia ='{3}' where idusuario = {4}", 
                usuario.Nombre, usuario.Apellidopaterno, usuario.Apellidomaterno, usuario.Contrasenia, 
                usuario.Idusuario);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idUsuario)
        {
            string consulta = string.Format("Delete from usuario where idusuario = {0}", idUsuario);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = new List<Usuario>();
            var ds = new DataSet();
            string consulta = "Select * from usuario where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "usuario");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var usuario = new Usuario
                {
                    Idusuario = Convert.ToInt32(row["idusuario"]),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Contrasenia = row["contrasenia"].ToString()
                };
                listUsuario.Add(usuario);
            }
            return listUsuario;
        }

    }
}
