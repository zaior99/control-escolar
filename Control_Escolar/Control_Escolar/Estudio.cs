﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;

namespace Control_Escolar
{
    public partial class Estudio : Form
    {
        private EstudioManejador _estudioManejador;
        private ProfesorManejador _profesorManejador;
        private OpenFileDialog _documentoPDF;
        private string _ruta;

        public Estudio()
        {
            InitializeComponent();

            _estudioManejador = new EstudioManejador();
            _profesorManejador = new ProfesorManejador();
            _documentoPDF = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\doc\\";
        }

        private void Estudio_Load(object sender, EventArgs e)
        {
            buscarestudio("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscarestudio(string filtro)
        {

            dtgEstudios.DataSource = _estudioManejador.GetEstudio(filtro);

        }
        private void TraerProfesor(string filtro)
        {
            cmbProfesor.DataSource = _profesorManejador.GetProfesor(filtro);
            cmbProfesor.DisplayMember = "nombre";
            cmbProfesor.ValueMember = "idprofesor";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbProfesor.Enabled = activar;
            txtEstudios.Enabled = activar;
            txtPDF.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            cmbProfesor.Text = "";
            txtEstudios.Text = "";
            txtPDF.Text = "";

        }
        private void GuardarEstudio()
        {
            _estudioManejador.Guardar(new Estudios
            {
                Idestudio = Convert.ToInt32(lblId.Text),
                Nombre = cmbProfesor.SelectedValue.ToString(),
                Estudio = txtEstudios.Text,              
                Documento = txtPDF.Text,


            });
        }
        private void EliminarEstudio()
        {
            var Idestudio = dtgEstudios.CurrentRow.Cells["idestudio"].Value;
            _estudioManejador.Eliminar(Convert.ToInt32(Idestudio));
        }
        private void ModificarEstudio()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgEstudios.CurrentRow.Cells["idestudio"].Value.ToString();
            cmbProfesor.Text = dtgEstudios.CurrentRow.Cells["nombre"].Value.ToString();
            txtEstudios.Text = dtgEstudios.CurrentRow.Cells["estudio"].Value.ToString();           
            txtPDF.Text = dtgEstudios.CurrentRow.Cells["documento"].Value.ToString();

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarestudio(txtBuscar.Text);
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbProfesor.Focus();
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarEstudio();
                GuardarPdf();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarEstudio();
                    buscarestudio("");
                    EliminarPDF();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
           
        }

        private void dtgEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarEstudio();
                buscarestudio("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cmbProfesor_Click(object sender, EventArgs e)
        {
            TraerProfesor("");
        }

        private void cmbProfesor_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void btnCargarPDF_Click(object sender, EventArgs e)
        {
            CargarPDF();
        }
        private void CargarPDF()
        {
            _documentoPDF.Filter = "Documento PDF tipo (*.pdf)|*.pdf";
            _documentoPDF.Title = "Cargar Documento PDF";
            _documentoPDF.ShowDialog();

            if (_documentoPDF.FileName != "")
            {
                var archivo = new FileInfo(_documentoPDF.FileName);

                txtPDF.Text = archivo.Name;
            }
        }

        private void btnEliminarPDF_Click(object sender, EventArgs e)
        {
            EliminarPDF();
            txtPDF.Text = "";
        }
        private void EliminarPDF()
        {           
            if (MessageBox.Show("Esta seguro de Eliminar esto", "Eliminar PDF", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta) )
                {
                    //codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                    FileInfo archivoAnterior;


                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }
                    }
                }
            }
        }

        private void btnGuardarPDF_Click(object sender, EventArgs e)
        {
            
        }
        private void GuardarPdf()
        {
            if (_documentoPDF.FileName != null)
            {
                if (_documentoPDF.FileName != "")
                {
                    var archivo = new FileInfo(_documentoPDF.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        //codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //ModificarEstudio();
        }
    }
}
