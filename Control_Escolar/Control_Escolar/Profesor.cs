﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace Control_Escolar
{
    public partial class Profesor : Form
    {
        private ProfesorManejador _profesorManejador;
        private EstadoManejador _estadoManejador;
        private CiudadesManejador _ciudadesManejador;

        public Profesor()
        {
            InitializeComponent();
            _profesorManejador = new ProfesorManejador();
            _estadoManejador = new EstadoManejador();
            _ciudadesManejador = new CiudadesManejador();
        }

        private void Profesor_Load(object sender, EventArgs e)
        {
            buscarprofesor("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            noControl();
        }
        private void buscarprofesor(string filtro)
        {

            dtgProfesor.DataSource = _profesorManejador.GetProfesor(filtro);
        }
        private void TraerEstados(string filtro)
        {
            cmbEstado.DataSource = _estadoManejador.GetEstados(filtro);
            cmbEstado.DisplayMember = "codigoestado";

        }
        private void TraerCiudades(string filtro)
        {
            cmbCiudad.DataSource = _ciudadesManejador.GetCiudades(cmbEstado.Text);
            cmbCiudad.DisplayMember = "nombre";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNoControl.Enabled = activar;
            txtNombre.Enabled = activar;
            txtAPaterno.Enabled = activar;
            txtAmaterno.Enabled = activar;            
            txtDireccion.Enabled = activar;  
            cmbCiudad.Enabled = activar;
            cmbEstado.Enabled = activar;
            txtNoCedula.Enabled = activar;
            txtTitulo.Enabled = activar;
            txtFechanacimiento.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNoControl.Text = "";
            txtNombre.Text = "";
            txtAPaterno.Text = "";
            txtAmaterno.Text = "";            
            txtDireccion.Text = "";
            cmbCiudad.Text = "";
            cmbEstado.Text = "";
            txtNoCedula.Text = "";
            txtTitulo.Text = "";
            txtFechanacimiento.Text = "";
        }
        private void GuardarProfesor()
        {
            _profesorManejador.Guardar(new Profesores
            {
                Idprofesor = Convert.ToInt32(lblId.Text),
                Nocontrolp = txtNoControl.Text,
                Nombre = txtNombre.Text,
                Apellidopaterno = txtAPaterno.Text,
                Apellidomaterno = txtAmaterno.Text,               
                Direccion = txtDireccion.Text,                              
                Ciudad = cmbCiudad.Text,
                Estado = cmbEstado.Text,
                Numerocedula = txtNoCedula.Text,
                Titulo = txtTitulo.Text,
                Fechanacimeinto = txtFechanacimiento.Text

            });
        }
        private void EliminarProfesor()
        {
            var Idprofesor = dtgProfesor.CurrentRow.Cells["idprofesor"].Value;
            _profesorManejador.Eliminar(Convert.ToInt32(Idprofesor));
        }
        private void ModificarProfesor()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgProfesor.CurrentRow.Cells["idprofesor"].Value.ToString();
            txtNoControl.Text = dtgProfesor.CurrentRow.Cells["nocontrolp"].Value.ToString();
            txtNombre.Text = dtgProfesor.CurrentRow.Cells["nombre"].Value.ToString();
            txtAPaterno.Text = dtgProfesor.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtAmaterno.Text = dtgProfesor.CurrentRow.Cells["apellidomaterno"].Value.ToString();           
            txtDireccion.Text = dtgProfesor.CurrentRow.Cells["Direccion"].Value.ToString();
            cmbCiudad.Text = dtgProfesor.CurrentRow.Cells["Ciudad"].Value.ToString();
            cmbEstado.Text = dtgProfesor.CurrentRow.Cells["Estado"].Value.ToString();
            txtNoCedula.Text = dtgProfesor.CurrentRow.Cells["numerocedula"].Value.ToString();
            txtTitulo.Text = dtgProfesor.CurrentRow.Cells["titulo"].Value.ToString();
            txtFechanacimiento.Text = dtgProfesor.CurrentRow.Cells["fechanacimeinto"].Value.ToString();


        }
        private void noControl()
        {
            DateTime d = DateTime.Now;
            int anio = d.Year;
            int id = 0;
            DataSet ds;
            ds = _profesorManejador.Nocontrol(anio.ToString());
            try
            {
                string a = ds.Tables[0].Rows[0]["nocontrolp"].ToString();
                int i = Convert.ToInt32(a) + 1;
                if (i >= 10)
                {
                    txtNoControl.Text = "D" + anio.ToString() + i;

                }
                else
                {
                    id++;
                    txtNoControl.Text = "D" + anio.ToString() + "0" + i;
                }

            }
            catch (Exception)
            {

                id++;
                txtNoControl.Text = "D" + anio.ToString() + "0" + id.ToString();
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarprofesor(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNoControl.Focus();
            noControl();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarProfesor();
                noControl();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarProfesor();
                    buscarprofesor("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgProfesor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarProfesor();
                buscarprofesor("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dtgProfesor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerCiudades("");
        }

        private void cmbCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbCiudad_Click(object sender, EventArgs e)
        {            
        }
        private void txtNoControl_TextChanged(object sender, EventArgs e)
        {
        }
        private void cmbEstado_Click(object sender, EventArgs e)
        {
            TraerEstados("");
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void txtNoControl_TextChanged_1(object sender, EventArgs e)
        {
           
        }
    }
}
