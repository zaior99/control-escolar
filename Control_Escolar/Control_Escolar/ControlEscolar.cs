﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace Control_Escolar
{
    public partial class frmControlEscolar : Form
    {
        private UsuarioManejador _usuarioManejador;
        private Usuario _usuario;

        public frmControlEscolar()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuario();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarusuario(txtBuscar.Text);
        }

        private void frmControlEscolar_Load(object sender, EventArgs e)
        {
            buscarusuario("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscarusuario(string filtro)
        {

            dtgUsuario.DataSource = _usuarioManejador.GetUsuarios(filtro);
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtAPaterno.Enabled = activar;
            txtAmaterno.Enabled = activar;
            txtContraseña.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtAPaterno.Text = "";
            txtAmaterno.Text = "";
            txtContraseña.Text = "";
        }
        private void CargarUsuario()
        {
            _usuario.Idusuario = Convert.ToInt32(lblId.Text);
            _usuario.Nombre = txtNombre.Text;
            _usuario.Apellidopaterno = txtAPaterno.Text;
            _usuario.Apellidomaterno = txtAmaterno.Text;
            _usuario.Contrasenia = txtContraseña.Text;
        }
        private void GuardarUsuario()
        {
            _usuarioManejador.Guardar(_usuario);
        }
        private void EliminarUsuario()
        {
            var Idusuario = dtgUsuario.CurrentRow.Cells["idusuario"].Value;
            _usuarioManejador.Eliminar(Convert.ToInt32(Idusuario));
        }
        private void ModificarUsuario()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgUsuario.CurrentRow.Cells["idusuario"].Value.ToString();
            txtNombre.Text = dtgUsuario.CurrentRow.Cells["nombre"].Value.ToString();
            txtAPaterno.Text = dtgUsuario.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtAmaterno.Text = dtgUsuario.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            txtContraseña.Text = dtgUsuario.CurrentRow.Cells["contrasenia"].Value.ToString();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }
        private bool ValidarUsuario()
        {
            var tupla = _usuarioManejador.ValidarUsuario(_usuario);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;

            if (!valido )
            {
                MessageBox.Show(mensaje, "Error de validacion" , MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            return valido;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {          
            try
            {
                CargarUsuario();
                if (ValidarUsuario())
                {
                   
                    GuardarUsuario();
                    LimpiarCuadros();
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false); 
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", 
                "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarUsuario();
                    buscarusuario("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgUsuario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuario();
                buscarusuario("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtAPaterno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
