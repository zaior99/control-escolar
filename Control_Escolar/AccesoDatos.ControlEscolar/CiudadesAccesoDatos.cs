﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CiudadesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CiudadesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }

        public List<Ciudades> GetCiudades(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listCiudades = new List<Ciudades>();
            var ds = new DataSet();
            string consulta = "Select * from Ciudades where fkcodigoestado like " +
                "'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Ciudades");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Ciudades
                {

                    Codigocuidad = Convert.ToInt32(row["codigociudad"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Fkcodigoestado = row["fkcodigoestado"].ToString()

                };
                listCiudades.Add(ciudades);
            }


            return listCiudades;
        }
    }
}
