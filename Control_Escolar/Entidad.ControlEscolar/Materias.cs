﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Materias
    {
        private int _idmateria;
        private string _nombremateria;
        private int _hrsteoria;
        private int _hrspractica;
        private int _creditos;

        public int Idmateria { get => _idmateria; set => _idmateria = value; }
        public string Nombremateria { get => _nombremateria; set => _nombremateria = value; }
        public int Hrsteoria { get => _hrsteoria; set => _hrsteoria = value; }
        public int Hrspractica { get => _hrspractica; set => _hrspractica = value; }
        public int Creditos { get => _creditos; set => _creditos = value; }
    }
}
