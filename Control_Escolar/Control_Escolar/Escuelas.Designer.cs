﻿namespace Control_Escolar
{
    partial class Escuelas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblId = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPaginaWeb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblidEscuela = new System.Windows.Forms.Label();
            this.txtDirector = new System.Windows.Forms.TextBox();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.lblContraseña = new System.Windows.Forms.Label();
            this.lblAmaterno = new System.Windows.Forms.Label();
            this.lblApaterno = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.mtxtTelefono = new System.Windows.Forms.MaskedTextBox();
            this.btnCargarJpg = new System.Windows.Forms.Button();
            this.btnEliminarJpj = new System.Windows.Forms.Button();
            this.dtgEscuela = new System.Windows.Forms.DataGridView();
            this.btnModifica = new System.Windows.Forms.Button();
            this.txtLogo = new System.Windows.Forms.PictureBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.txtNombreImagen = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEscuela)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(785, 9);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(22, 25);
            this.lblId.TabIndex = 92;
            this.lblId.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(611, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 91;
            this.label4.Text = "Logo";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(12, 242);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(203, 25);
            this.txtEmail.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 90;
            this.label3.Text = "Email";
            // 
            // txtPaginaWeb
            // 
            this.txtPaginaWeb.Location = new System.Drawing.Point(383, 243);
            this.txtPaginaWeb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPaginaWeb.Name = "txtPaginaWeb";
            this.txtPaginaWeb.Size = new System.Drawing.Size(424, 25);
            this.txtPaginaWeb.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(380, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 89;
            this.label2.Text = "Pagina Web";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(12, 72);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(574, 25);
            this.txtNombre.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 88;
            this.label1.Text = "Nombre";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(38, 14);
            this.txtId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(28, 25);
            this.txtId.TabIndex = 69;
            this.txtId.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // lblidEscuela
            // 
            this.lblidEscuela.AutoSize = true;
            this.lblidEscuela.Location = new System.Drawing.Point(12, 17);
            this.lblidEscuela.Name = "lblidEscuela";
            this.lblidEscuela.Size = new System.Drawing.Size(20, 17);
            this.lblidEscuela.TabIndex = 87;
            this.lblidEscuela.Text = "ID";
            // 
            // txtDirector
            // 
            this.txtDirector.Location = new System.Drawing.Point(12, 122);
            this.txtDirector.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDirector.Name = "txtDirector";
            this.txtDirector.Size = new System.Drawing.Size(574, 25);
            this.txtDirector.TabIndex = 2;
            this.txtDirector.TextChanged += new System.EventHandler(this.txtDirector_TextChanged);
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Location = new System.Drawing.Point(251, 190);
            this.txtDomicilio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.Size = new System.Drawing.Size(556, 25);
            this.txtDomicilio.TabIndex = 4;
            // 
            // txtRFC
            // 
            this.txtRFC.Location = new System.Drawing.Point(12, 190);
            this.txtRFC.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(203, 25);
            this.txtRFC.TabIndex = 3;
            this.txtRFC.TextChanged += new System.EventHandler(this.txtRFC_TextChanged);
            // 
            // lblContraseña
            // 
            this.lblContraseña.AutoSize = true;
            this.lblContraseña.Location = new System.Drawing.Point(9, 101);
            this.lblContraseña.Name = "lblContraseña";
            this.lblContraseña.Size = new System.Drawing.Size(55, 17);
            this.lblContraseña.TabIndex = 85;
            this.lblContraseña.Text = "Director";
            // 
            // lblAmaterno
            // 
            this.lblAmaterno.AutoSize = true;
            this.lblAmaterno.Location = new System.Drawing.Point(248, 225);
            this.lblAmaterno.Name = "lblAmaterno";
            this.lblAmaterno.Size = new System.Drawing.Size(59, 17);
            this.lblAmaterno.TabIndex = 84;
            this.lblAmaterno.Text = "Telefono";
            // 
            // lblApaterno
            // 
            this.lblApaterno.AutoSize = true;
            this.lblApaterno.Location = new System.Drawing.Point(248, 169);
            this.lblApaterno.Name = "lblApaterno";
            this.lblApaterno.Size = new System.Drawing.Size(62, 17);
            this.lblApaterno.TabIndex = 83;
            this.lblApaterno.Text = "Domicilio";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(9, 169);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(30, 17);
            this.lblNombre.TabIndex = 82;
            this.lblNombre.Text = "RFC";
            // 
            // mtxtTelefono
            // 
            this.mtxtTelefono.Location = new System.Drawing.Point(251, 243);
            this.mtxtTelefono.Mask = "000-000-0000";
            this.mtxtTelefono.Name = "mtxtTelefono";
            this.mtxtTelefono.Size = new System.Drawing.Size(97, 25);
            this.mtxtTelefono.TabIndex = 6;
            this.mtxtTelefono.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // btnCargarJpg
            // 
            this.btnCargarJpg.Location = new System.Drawing.Point(759, 63);
            this.btnCargarJpg.Name = "btnCargarJpg";
            this.btnCargarJpg.Size = new System.Drawing.Size(48, 23);
            this.btnCargarJpg.TabIndex = 112;
            this.btnCargarJpg.Text = "...";
            this.btnCargarJpg.UseVisualStyleBackColor = true;
            this.btnCargarJpg.Click += new System.EventHandler(this.btnCargarJpg_Click);
            // 
            // btnEliminarJpj
            // 
            this.btnEliminarJpj.Location = new System.Drawing.Point(759, 114);
            this.btnEliminarJpj.Name = "btnEliminarJpj";
            this.btnEliminarJpj.Size = new System.Drawing.Size(48, 23);
            this.btnEliminarJpj.TabIndex = 113;
            this.btnEliminarJpj.Text = "x";
            this.btnEliminarJpj.UseVisualStyleBackColor = true;
            this.btnEliminarJpj.Click += new System.EventHandler(this.btnEliminarPDF_Click);
            // 
            // dtgEscuela
            // 
            this.dtgEscuela.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEscuela.Location = new System.Drawing.Point(123, 341);
            this.dtgEscuela.Name = "dtgEscuela";
            this.dtgEscuela.Size = new System.Drawing.Size(92, 68);
            this.dtgEscuela.TabIndex = 115;
            this.dtgEscuela.Visible = false;
            // 
            // btnModifica
            // 
            this.btnModifica.Image = global::Control_Escolar.Properties.Resources.icons8_edit_account_50__2_;
            this.btnModifica.Location = new System.Drawing.Point(636, 278);
            this.btnModifica.Name = "btnModifica";
            this.btnModifica.Size = new System.Drawing.Size(54, 43);
            this.btnModifica.TabIndex = 116;
            this.btnModifica.UseVisualStyleBackColor = true;
            this.btnModifica.Click += new System.EventHandler(this.btnModifica_Click);
            // 
            // txtLogo
            // 
            this.txtLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLogo.Location = new System.Drawing.Point(614, 65);
            this.txtLogo.Name = "txtLogo";
            this.txtLogo.Size = new System.Drawing.Size(121, 82);
            this.txtLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.txtLogo.TabIndex = 114;
            this.txtLogo.TabStop = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = global::Control_Escolar.Properties.Resources.icons8_remove_64;
            this.btnEliminar.Location = new System.Drawing.Point(758, 276);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(56, 45);
            this.btnEliminar.TabIndex = 86;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::Control_Escolar.Properties.Resources.icons8_cancel_64__1_;
            this.btnCancelar.Location = new System.Drawing.Point(696, 276);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(56, 45);
            this.btnCancelar.TabIndex = 80;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::Control_Escolar.Properties.Resources.icons8_save_close_40__1_;
            this.btnGuardar.Location = new System.Drawing.Point(574, 278);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(56, 43);
            this.btnGuardar.TabIndex = 79;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Image = global::Control_Escolar.Properties.Resources.icons8_add_64;
            this.btnNuevo.Location = new System.Drawing.Point(512, 278);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(56, 43);
            this.btnNuevo.TabIndex = 78;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // txtNombreImagen
            // 
            this.txtNombreImagen.Location = new System.Drawing.Point(614, 154);
            this.txtNombreImagen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNombreImagen.Name = "txtNombreImagen";
            this.txtNombreImagen.Size = new System.Drawing.Size(121, 25);
            this.txtNombreImagen.TabIndex = 117;
            // 
            // Escuelas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 329);
            this.Controls.Add(this.txtNombreImagen);
            this.Controls.Add(this.btnModifica);
            this.Controls.Add(this.dtgEscuela);
            this.Controls.Add(this.txtLogo);
            this.Controls.Add(this.btnCargarJpg);
            this.Controls.Add(this.btnEliminarJpj);
            this.Controls.Add(this.mtxtTelefono);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPaginaWeb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblidEscuela);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.txtDirector);
            this.Controls.Add(this.txtDomicilio);
            this.Controls.Add(this.txtRFC);
            this.Controls.Add(this.lblContraseña);
            this.Controls.Add(this.lblAmaterno);
            this.Controls.Add(this.lblApaterno);
            this.Controls.Add(this.lblNombre);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Escuelas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Escuela";
            this.Load += new System.EventHandler(this.Escuela_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgEscuela)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPaginaWeb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblidEscuela;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.TextBox txtDirector;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.TextBox txtRFC;
        private System.Windows.Forms.Label lblContraseña;
        private System.Windows.Forms.Label lblAmaterno;
        private System.Windows.Forms.Label lblApaterno;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.MaskedTextBox mtxtTelefono;
        private System.Windows.Forms.Button btnCargarJpg;
        private System.Windows.Forms.Button btnEliminarJpj;
        public System.Windows.Forms.PictureBox txtLogo;
        private System.Windows.Forms.DataGridView dtgEscuela;
        private System.Windows.Forms.Button btnModifica;
        private System.Windows.Forms.TextBox txtNombreImagen;
    }
}