use empresas;

drop table usuario;

create table usuario(idusuario int primary key AUTO_INCREMENT, 
nombre varchar(50), 
apellidopaterno varchar(50),
apellidomaterno varchar(50),
contrasenia varchar(20));

insert into usuario values(null, 'Zaira','Olmos','Rodriguez','1234');

select * from usuario;

create table alumno(idalumno int primary key AUTO_INCREMENT,
nocontrol varchar(50), 
nombre varchar(50),
apellidopaterno varchar(50),
apellidomaterno varchar(50),
fechanacimiento varchar(50),
domicilio varchar(100),
email varchar(50),
sexo varchar(50));

drop table alumno;

insert into alumno values(null, '12345', 'Zaira','Olmos','Rodriguez','1999-05-29', 'Arboledas del Carmen #32', 'zaior99@hotamil.com','F');

select * from alumno;

create table estado(codigoestado varchar(20) primary key,
nombre  varchar(50));

drop table estado;
SELECT * FROM estado;

Insert into estado values('AGU','Aguascalientes');			
Insert into estado values('BCN','Baja California Norte');			
Insert into estado values('BCS','Baja Clifornia Sur');			
Insert into estado values('CAM','Campeche');			
Insert into estado values('CHP','Chiapas');			
Insert into estado values('CHH','Chihuahua');			
Insert into estado values('CMX','Ciudad de México');			
Insert into estado values('COA','Coahuila');			
Insert into estado values('COL','Colima');			
Insert into estado values('DUR','Durango');			
Insert into estado values('GUA','Guanajuato');			
Insert into estado values('GRO','Guerrero');			
Insert into estado values('HID','Hidalgo');			
Insert into estado values('JAL','Jalisco');			
Insert into estado values('MEX','México');			
Insert into estado values('MIC','Michoacán');			
Insert into estado values('MOR','Morelos');			
Insert into estado values('NAY','Nayarit');			
Insert into estado values('NLE','Nuevo León');			
Insert into estado values('OAX','Oaxaca');			
Insert into estado values('PUE','Puebla');			
Insert into estado values('QUE','Querétaro');			
Insert into estado values('ROO','Quintana Roo');			
Insert into estado values('SLP','San Luis Potosí');			
Insert into estado values('SIN','Sinaloa');			
Insert into estado values('SON','Sonora');			
Insert into estado values('TAB','Tabasco');			
Insert into estado values('TAM','Tamaulipas');			
Insert into estado values('TLA','Tlaxcala');			
Insert into estado values('VER','Veracruz');			
Insert into estado values('YUC','Yucatán');			
Insert into estado values('ZAC','Zacatecas');			

describe estado;

create table ciudad(codigocuidad int primary key AUTO_INCREMENT,
ciudad  varchar (50),
fkestado varchar(20),
foreign key(fkestado) references estado(codigoestado));

drop table ciudad;

describe ciudad;

select * from ciudad;

insert into ciudad values(null,'Aguascalientes','AGU');
insert into ciudad values(null,'Jesús María','AGU');
insert into ciudad values(null,'Rincón de Romos','AGU');
insert into ciudad values(null,'Calvillo','AGU');
insert into ciudad values(null,'Asientos','AGU');
insert into ciudad values(null,'Pabellón de Arteaga','AGU');
insert into ciudad values(null,'San Francisco de los Romo','AGU');
insert into ciudad values(null,'Tepezalá','AGU');
insert into ciudad values(null,'El Llano','AGU');
insert into ciudad values(null,'Cosío','AGU');
insert into ciudad values(null,'San José de Gracia','AGU');

insert into ciudad values(null,'Mexicali','BCN');
insert into ciudad values(null,'Tijuana','BCN');
insert into ciudad values(null,'Ensenada','BCN');
insert into ciudad values(null,'Tecate','BCN');
insert into ciudad values(null,'Rosarito','BCN');

insert into ciudad values(null,'La Paz','BCS');
insert into ciudad values(null,'Los Cabos','BCS');
insert into ciudad values(null,'Loreto','BCS');
insert into ciudad values(null,'Mulegé','BCS');
insert into ciudad values(null,'Comondú','BCS');

insert into ciudad values(null,'Calakmul','CAM');
insert into ciudad values(null,'Calkiní','CAM');
insert into ciudad values(null,'Campeche','CAM');
insert into ciudad values(null,'Candelaria','CAM');
insert into ciudad values(null,'Carmen','CAM');
insert into ciudad values(null,'Champotón','CAM');
insert into ciudad values(null,'Escárcega','CAM');
insert into ciudad values(null,'Hecelchakán','CAM');
insert into ciudad values(null,'Hopelchén','CAM');
insert into ciudad values(null,'Palizada','CAM');
insert into ciudad values(null,'Tenabo','CAM');


insert into ciudad values(null,'Acacoyagua','CHP');
insert into ciudad values(null,'Acala','CHP');
insert into ciudad values(null,'Acapetahua','CHP');
insert into ciudad values(null,'Aldama','CHP');
insert into ciudad values(null,'Altamirano','CHP');
insert into ciudad values(null,'Amatán','CHP');
insert into ciudad values(null,'Amatenango de la Frontera','CHP');
insert into ciudad values(null,'Amatenango del Valle','CHP');
insert into ciudad values(null,'Ángel Albino Corzo','CHP');
insert into ciudad values(null,'Arriaga','CHP');
insert into ciudad values(null,'Bejucal de Ocampo','CHP');
insert into ciudad values(null,'Bella Vista','CHP');
insert into ciudad values(null,'Benemérito de las Américas','CHP');
insert into ciudad values(null,'Berriozábal','CHP');
insert into ciudad values(null,'Bochil','CHP');
insert into ciudad values(null,'Cacahoatán','CHP');
insert into ciudad values(null,'Chalchihuitán','CHP');
insert into ciudad values(null,'Chamula','CHP');
insert into ciudad values(null,'Chanal','CHP');
insert into ciudad values(null,'Chapultenango','CHP');
insert into ciudad values(null,'Catazajá','CHP');
insert into ciudad values(null,'Chenalhó','CHP');
insert into ciudad values(null,'Chiapa de Corzo','CHP');
insert into ciudad values(null,'Chiapilla','CHP');
insert into ciudad values(null,'Chicoasén','CHP');
insert into ciudad values(null,'Chicomosuelo','CHP');
insert into ciudad values(null,'Cintalpa','CHP');
insert into ciudad values(null,'Chilón','CHP');
insert into ciudad values(null,'Coapilla','CHP');
insert into ciudad values(null,'Comitán de Domínguez','CHP');
insert into ciudad values(null,'Copainalá','CHP');
insert into ciudad values(null,'El Bosque','CHP');
insert into ciudad values(null,'El Porvenir','CHP');
insert into ciudad values(null,'Escuintla','CHP');
insert into ciudad values(null,'Francisco León','CHP');
insert into ciudad values(null,'Frontera Comalapa','CHP');
insert into ciudad values(null,'Frontera Hidalgo','CHP');
insert into ciudad values(null,'Huehuetán','CHP');
insert into ciudad values(null,'Huitiupán','CHP');
insert into ciudad values(null,'Huixtán','CHP');
insert into ciudad values(null,'Huixtla','CHP');
insert into ciudad values(null,'Ixhuatán','CHP');
insert into ciudad values(null,'Ixtacomitán','CHP');
insert into ciudad values(null,'Ixtapa','CHP');
insert into ciudad values(null,'Ixtapangajoya','CHP');
insert into ciudad values(null,'Jiquipilas','CHP');
insert into ciudad values(null,'Jitotol','CHP');
insert into ciudad values(null,'Juárez','CHP');
insert into ciudad values(null,'La Concordia','CHP');
insert into ciudad values(null,'La Grandeza','CHP');
insert into ciudad values(null,'La Independencia','CHP');
insert into ciudad values(null,'La Libertad','CHP');
insert into ciudad values(null,'La Trinitaria','CHP');
insert into ciudad values(null,'Larráinzar','CHP');
insert into ciudad values(null,'Las Margaritas','CHP');
insert into ciudad values(null,'Las Rosas','CHP');
insert into ciudad values(null,'Mapastepec','CHP');
insert into ciudad values(null,'Maravilla Tenejapa','CHP');
insert into ciudad values(null,'Marqués de Comillas','CHP');
insert into ciudad values(null,'Mazapa de Madero','CHP');
insert into ciudad values(null,'Mazatán','CHP');
insert into ciudad values(null,'Metapa','CHP');
insert into ciudad values(null,'Mitontic','CHP');
insert into ciudad values(null,'Montecristo de Guerrero','CHP');
insert into ciudad values(null,'Motozintla','CHP');
insert into ciudad values(null,'Nicolás Ruíz','CHP');
insert into ciudad values(null,'Ocosingo','CHP');
insert into ciudad values(null,'Ocotepec','CHP');
insert into ciudad values(null,'Ocozocoautla de Espinosa','CHP');
insert into ciudad values(null,'Ostuacán','CHP');
insert into ciudad values(null,'Osumacinta','CHP');
insert into ciudad values(null,'Oxchuc','CHP');
insert into ciudad values(null,'Palenque','CHP');
insert into ciudad values(null,'Pantelhó','CHP');
insert into ciudad values(null,'Pantepec','CHP');
insert into ciudad values(null,'Pichucalco','CHP');
insert into ciudad values(null,'Pijijiapan','CHP');
insert into ciudad values(null,'Pueblo Nuevo Solistahuacán','CHP');
insert into ciudad values(null,'Rayón','CHP');
insert into ciudad values(null,'Reforma','CHP');
insert into ciudad values(null,'Sabanilla','CHP');
insert into ciudad values(null,'Salto de Agua','CHP');
insert into ciudad values(null,'San Andrés Duraznal','CHP');
insert into ciudad values(null,'San Cristóbal de las Casas','CHP');
insert into ciudad values(null,'San Fernando','CHP');
insert into ciudad values(null,'San Juan Cancuc','CHP');
insert into ciudad values(null,'San Lucas','CHP');
insert into ciudad values(null,'Santiago el Pinar','CHP');
insert into ciudad values(null,'Siltepec','CHP');
insert into ciudad values(null,'Simojovel','CHP');
insert into ciudad values(null,'Sitalá','CHP');
insert into ciudad values(null,'Socoltenango','CHP');
insert into ciudad values(null,'Solosuchiapa','CHP');
insert into ciudad values(null,'Soyaló','CHP');
insert into ciudad values(null,'Suchiapa','CHP');
insert into ciudad values(null,'Suchiate','CHP');
insert into ciudad values(null,'Sunuapa','CHP');
insert into ciudad values(null,'Tapachula','CHP');
insert into ciudad values(null,'Tapalapa','CHP');
insert into ciudad values(null,'Tapilula','CHP');
insert into ciudad values(null,'Tecpatán','CHP');
insert into ciudad values(null,'Tenejapa','CHP');
insert into ciudad values(null,'Teopisca','CHP');
insert into ciudad values(null,'Tila','CHP');
insert into ciudad values(null,'Tonalá','CHP');
insert into ciudad values(null,'Totolapa','CHP');
insert into ciudad values(null,'Tumbalá','CHP');
insert into ciudad values(null,'Tuxtla Chico','CHP');
insert into ciudad values(null,'Tuxtla Gutiérrez','CHP');
insert into ciudad values(null,'Tuzantán','CHP');
insert into ciudad values(null,'Tzimol','CHP');
insert into ciudad values(null,'Unión Juárez','CHP');
insert into ciudad values(null,'Venustiano Carranza','CHP');
insert into ciudad values(null,'Villa Comaltitlán','CHP');
insert into ciudad values(null,'Villa Corzo','CHP');
insert into ciudad values(null,'Villaflores','CHP');
insert into ciudad values(null,'Yajalón','CHP');
insert into ciudad values(null,'Zinacantán','CHP');

insert into ciudad values(null,'Ahumada','CHH');
insert into ciudad values(null,'Aldama','CHH');
insert into ciudad values(null,'Allende','CHH');
insert into ciudad values(null,'Aquiles Serdán','CHH');
insert into ciudad values(null,'Ascensión','CHH');
insert into ciudad values(null,'Bachíniva','CHH');
insert into ciudad values(null,'Balleza','CHH');
insert into ciudad values(null,'Batopilas','CHH');
insert into ciudad values(null,'Bocoyna','CHH');
insert into ciudad values(null,'Buenaventura','CHH');
insert into ciudad values(null,'Camargo','CHH');
insert into ciudad values(null,'Carichí','CHH');
insert into ciudad values(null,'Casas Grandes','CHH');
insert into ciudad values(null,'Chihuahua','CHH');
insert into ciudad values(null,'Chínipas','CHH');
insert into ciudad values(null,'Coronado','CHH');
insert into ciudad values(null,'Coyame del Sotol','CHH');
insert into ciudad values(null,'Cuauhtémoc','CHH');
insert into ciudad values(null,'Cusihuiriachi','CHH');
insert into ciudad values(null,'Delicias','CHH');
insert into ciudad values(null,'Dr. Belisario Domínguez','CHH');
insert into ciudad values(null,'El Tule','CHH');
insert into ciudad values(null,'Galeana','CHH');
insert into ciudad values(null,'Gómez Farías','CHH');
insert into ciudad values(null,'Gran Morelos','CHH');
insert into ciudad values(null,'Guachochi','CHH');
insert into ciudad values(null,'Guadalupe D.B.','CHH');
insert into ciudad values(null,'Guadalupe y Calvo','CHH');
insert into ciudad values(null,'Guazapares','CHH');
insert into ciudad values(null,'Guerrero','CHH');
insert into ciudad values(null,'Hidalgo del Parral','CHH');
insert into ciudad values(null,'Huejoitán','CHH');
insert into ciudad values(null,'Ignacio Zaragoza','CHH');
insert into ciudad values(null,'Janos','CHH');
insert into ciudad values(null,'Jiménez','CHH');
insert into ciudad values(null,'Juárez','CHH');
insert into ciudad values(null,'Julimes','CHH');
insert into ciudad values(null,'La Cruz','CHH');
insert into ciudad values(null,'López','CHH');
insert into ciudad values(null,'Madera','CHH');
insert into ciudad values(null,'Maguarichi','CHH');
insert into ciudad values(null,'Manuel Benavides','CHH');
insert into ciudad values(null,'Matachí','CHH');
insert into ciudad values(null,'Matamoros','CHH');
insert into ciudad values(null,'Meoqui','CHH');
insert into ciudad values(null,'Morelos','CHH');
insert into ciudad values(null,'Moris','CHH');
insert into ciudad values(null,'Namiquipa','CHH');
insert into ciudad values(null,'Nonoava','CHH');
insert into ciudad values(null,'Nuevo Casas Grandes','CHH');
insert into ciudad values(null,'Ocampo','CHH');
insert into ciudad values(null,'Ojinaga','CHH');
insert into ciudad values(null,'Praxedis G. Guerrero','CHH');
insert into ciudad values(null,'Riva Palacio','CHH');
insert into ciudad values(null,'Rosales','CHH');
insert into ciudad values(null,'Rosario','CHH');
insert into ciudad values(null,'San Francisco de Borja','CHH');
insert into ciudad values(null,'San Francisco de Conchos','CHH');
insert into ciudad values(null,'San Francisco del Oro','CHH');
insert into ciudad values(null,'Santa Bárabara','CHH');
insert into ciudad values(null,'Santa Isabel','CHH');
insert into ciudad values(null,'Satevó','CHH');
insert into ciudad values(null,'Saucillo','CHH');
insert into ciudad values(null,'Temósachi','CHH');
insert into ciudad values(null,'Urique','CHH');
insert into ciudad values(null,'Uriachi','CHH');
insert into ciudad values(null,'Valle de Zaragoza','CHH');

insert into ciudad values(null,'Álvaro Obregón','CMX');
insert into ciudad values(null,'Azcapotzalco','CMX');
insert into ciudad values(null,'Benito Juárez','CMX');
insert into ciudad values(null,'Coyoacán','CMX');
insert into ciudad values(null,'Cuajimalpa de Morelos','CMX');
insert into ciudad values(null,'Cuauhtémoc','CMX');
insert into ciudad values(null,'Gustavo A. Madero','CMX');
insert into ciudad values(null,'Iztacalco','CMX');
insert into ciudad values(null,'Iztapalapa','CMX');
insert into ciudad values(null,'Magdalena Contreras','CMX');
insert into ciudad values(null,'Miguel Hidalgo','CMX');
insert into ciudad values(null,'Milpa Alta','CMX');
insert into ciudad values(null,'Tláhuac','CMX');
insert into ciudad values(null,'Tlalpan','CMX');
insert into ciudad values(null,'Venustiano Carranza','CMX');
insert into ciudad values(null,'Xochimilco','CMX');

insert into ciudad values(null,'Abasolo','COA');
insert into ciudad values(null,'Acuña','COA');
insert into ciudad values(null,'Allende','COA');
insert into ciudad values(null,'Arteaga','COA');
insert into ciudad values(null,'Candela','COA');
insert into ciudad values(null,'Castaños','COA');
insert into ciudad values(null,'Cuatrociénegas','COA');
insert into ciudad values(null,'Escobedo','COA');
insert into ciudad values(null,'Francisco I. Madero','COA');
insert into ciudad values(null,'Frontera','COA');
insert into ciudad values(null,'General Cepeda','COA');
insert into ciudad values(null,'Guerrero','COA');
insert into ciudad values(null,'Hidalgo','COA');
insert into ciudad values(null,'Jiménez','COA');
insert into ciudad values(null,'Juárez','COA');
insert into ciudad values(null,'Lamadrid','COA');
insert into ciudad values(null,'Matamoros','COA');
insert into ciudad values(null,'Monclova','COA');
insert into ciudad values(null,'Morelos','COA');
insert into ciudad values(null,'Múzquiz','COA');
insert into ciudad values(null,'Nadadores','COA');
insert into ciudad values(null,'Nava','COA');
insert into ciudad values(null,'Ocampo','COA');
insert into ciudad values(null,'Parras','COA');
insert into ciudad values(null,'Piedras Negras','COA');
insert into ciudad values(null,'Progreso','COA');
insert into ciudad values(null,'Ramos Arizpe','COA');
insert into ciudad values(null,'Sabinas','COA');
insert into ciudad values(null,'Sacramento','COA');
insert into ciudad values(null,'Saltillo','COA');
insert into ciudad values(null,'San Buenaventura','COA');
insert into ciudad values(null,'San Juan de Sabinas','COA');
insert into ciudad values(null,'San Pedro','COA');
insert into ciudad values(null,'Sierra Mojada','COA');
insert into ciudad values(null,'Torreón','COA');
insert into ciudad values(null,'Viesca','COA');
insert into ciudad values(null,'Villa Unión','COA');
insert into ciudad values(null,'Zaragoza','COA');

insert into ciudad values(null,'Armería','COL');
insert into ciudad values(null,'Colima','COL');
insert into ciudad values(null,'Comala','COL');
insert into ciudad values(null,'Coquimatlán','COL');
insert into ciudad values(null,'Cuauhtémoc','COL');
insert into ciudad values(null,'Ixtlahuacán','COL');
insert into ciudad values(null,'Manzanillo','COL');
insert into ciudad values(null,'Minatitlán','COL');
insert into ciudad values(null,'Tecomán','COL');
insert into ciudad values(null,'Villa de Álvarez','COL');

insert into ciudad values(null,'Canatlán','DUR');
insert into ciudad values(null,'Canelas','DUR');
insert into ciudad values(null,'Coneto de Comonfort','DUR');
insert into ciudad values(null,'Cuencamé','DUR');
insert into ciudad values(null,'Durango','DUR');
insert into ciudad values(null,'El Oro','DUR');
insert into ciudad values(null,'Gómez Palacio','DUR');
insert into ciudad values(null,'Gral. Simón Bolívar','DUR');
insert into ciudad values(null,'Guadalupe Victoria','DUR');
insert into ciudad values(null,'Guanaceví','DUR');
insert into ciudad values(null,'Hidalgo','DUR');
insert into ciudad values(null,'Indé','DUR');
insert into ciudad values(null,'Lerdo','DUR');
insert into ciudad values(null,'Mapimí','DUR');
insert into ciudad values(null,'Mezquital','DUR');
insert into ciudad values(null,'Nazas','DUR');
insert into ciudad values(null,'Nombre de Dios','DUR');
insert into ciudad values(null,'Nuevo Ideal','DUR');
insert into ciudad values(null,'Ocampo','DUR');
insert into ciudad values(null,'Otáez','DUR');
insert into ciudad values(null,'Pánuco de Coronado','DUR');
insert into ciudad values(null,'Peñón Blanco','DUR');
insert into ciudad values(null,'Poanas','DUR');
insert into ciudad values(null,'Pueblo Nuevo','DUR');
insert into ciudad values(null,'Rodeo','DUR');
insert into ciudad values(null,'San Bernardo','DUR');
insert into ciudad values(null,'San Dimas','DUR');
insert into ciudad values(null,'San Juan de Guadalupe','DUR');
insert into ciudad values(null,'San Juan del Río','DUR');
insert into ciudad values(null,'San Luis del Cordero','DUR');
insert into ciudad values(null,'San Pedro del Gallo','DUR');
insert into ciudad values(null,'Santa Clara','DUR');
insert into ciudad values(null,'Santiago Papasquiaro','DUR');
insert into ciudad values(null,'Súchil','DUR');
insert into ciudad values(null,'Tamazula','DUR');
insert into ciudad values(null,'Tepehuanes','DUR');
insert into ciudad values(null,'Tlahualilo','DUR');
insert into ciudad values(null,'Topia','DUR');
insert into ciudad values(null,'Vicente Guerrero','DUR');

insert into ciudad values(null,'Abasolo','GUA');
insert into ciudad values(null,'Acámbaro','GUA');
insert into ciudad values(null,'Apaseo el Alto','GUA');
insert into ciudad values(null,'Apaseo el Grande','GUA');
insert into ciudad values(null,'Atarjea','GUA');
insert into ciudad values(null,'Celaya','GUA');
insert into ciudad values(null,'Comonfort','GUA');
insert into ciudad values(null,'Coroneo','GUA');
insert into ciudad values(null,'Cortazar','GUA');
insert into ciudad values(null,'Cuerámaro','GUA');
insert into ciudad values(null,'Doctor Mora','GUA');
insert into ciudad values(null,'Dolores Hidalgo','GUA');
insert into ciudad values(null,'Guanajuato','GUA');
insert into ciudad values(null,'Huanímaro','GUA');
insert into ciudad values(null,'Irapuato','GUA');
insert into ciudad values(null,'Jaral del Progreso','GUA');
insert into ciudad values(null,'Jerécuaro','GUA');
insert into ciudad values(null,'León','GUA');
insert into ciudad values(null,'Manuel Doblado','GUA');
insert into ciudad values(null,'Moroleón','GUA');
insert into ciudad values(null,'Ocampo','GUA');
insert into ciudad values(null,'Pénjamo','GUA');
insert into ciudad values(null,'Pueblo Nuevo','GUA');
insert into ciudad values(null,'Purísima del Rincón','GUA');
insert into ciudad values(null,'Romita','GUA');
insert into ciudad values(null,'Salamanca','GUA');
insert into ciudad values(null,'Salvatierra','GUA');
insert into ciudad values(null,'San Diego de la Unión','GUA');
insert into ciudad values(null,'San Felipe','GUA');
insert into ciudad values(null,'San Francisco del Rincón','GUA');
insert into ciudad values(null,'San José Iturbide','GUA');
insert into ciudad values(null,'San Luis de la Paz','GUA');
insert into ciudad values(null,'San Miguel de Allende','GUA');
insert into ciudad values(null,'Santa Catarina','GUA');
insert into ciudad values(null,'Santa Cruz de Juventino','GUA');
insert into ciudad values(null,'Santiago Maravatío','GUA');
insert into ciudad values(null,'Silao','GUA');
insert into ciudad values(null,'Tarandacuao','GUA');
insert into ciudad values(null,'Tarimoro','GUA');
insert into ciudad values(null,'Tierra Blanca','GUA');
insert into ciudad values(null,'Uriangato','GUA');
insert into ciudad values(null,'Valle de Santiago','GUA');
insert into ciudad values(null,'Victoria','GUA');
insert into ciudad values(null,'Villagrán','GUA');
insert into ciudad values(null,'Xichú','GUA');
insert into ciudad values(null,'Yuriria','GUA');

insert into ciudad values(null,'Acapulco de Juárez','GRO');
insert into ciudad values(null,'Acatepec','GRO');
insert into ciudad values(null,'Ahuacuotzingo','GRO');
insert into ciudad values(null,'Ajuchitlán del Progreso','GRO');
insert into ciudad values(null,'Alcozauca de Guerrero','GRO');
insert into ciudad values(null,'Alpoyeca','GRO');
insert into ciudad values(null,'Apaxtla de Castrejón','GRO');
insert into ciudad values(null,'Arcelia','GRO');
insert into ciudad values(null,'Atenango del Río','GRO');
insert into ciudad values(null,'Atlamajalcingo del Monte','GRO');
insert into ciudad values(null,'Atlixtac','GRO');
insert into ciudad values(null,'Atoyac de Álvarez','GRO');
insert into ciudad values(null,'Ayutla de los Libres','GRO');
insert into ciudad values(null,'Azoyú','GRO');
insert into ciudad values(null,'Benito Juárez','GRO');
insert into ciudad values(null,'Buenavista de Cuéllar','GRO');
insert into ciudad values(null,'Chilapa de Álvarez','GRO');
insert into ciudad values(null,'Chilpancingo de los Bravo','GRO');
insert into ciudad values(null,'Coahuayutla de José María Izazaga','GRO');
insert into ciudad values(null,'Cochoapa el Grande','GRO');
insert into ciudad values(null,'Cocula','GRO');
insert into ciudad values(null,'Copala','GRO');
insert into ciudad values(null,'Copalillo','GRO');
insert into ciudad values(null,'Copanatoyac','GRO');
insert into ciudad values(null,'Coyuca de Benítez','GRO');
insert into ciudad values(null,'Coyuca de Catalán','GRO');
insert into ciudad values(null,'Cuajinicuilapa','GRO');
insert into ciudad values(null,'Cualác','GRO');
insert into ciudad values(null,'Cuautepec','GRO');
insert into ciudad values(null,'Cuetzala del Progreso','GRO');
insert into ciudad values(null,'Cutzamala de Pinzón','GRO');
insert into ciudad values(null,'Eduardo Neri','GRO');
insert into ciudad values(null,'Florencio Villarreal','GRO');
insert into ciudad values(null,'General Canuto A. Neri','GRO');
insert into ciudad values(null,'General Heliodoro Castillo','GRO');
insert into ciudad values(null,'Huamuxtitlán','GRO');
insert into ciudad values(null,'Huitzuco de los Figueroa','GRO');
insert into ciudad values(null,'Iguala de la Independencia','GRO');
insert into ciudad values(null,'Igualapa','GRO');
insert into ciudad values(null,'Iliatenco','GRO');
insert into ciudad values(null,'Ixcateopan de Cuauhtémoc','GRO');
insert into ciudad values(null,'José Joaquín de Herrera','GRO');
insert into ciudad values(null,'Juan R. Escudero','GRO');
insert into ciudad values(null,'Juchitán','GRO');
insert into ciudad values(null,'La Unión de Isidoro Montes de Oca','GRO');
insert into ciudad values(null,'Leonardo Bravo','GRO');
insert into ciudad values(null,'Malinaltepec','GRO');
insert into ciudad values(null,'Marquelia','GRO');
insert into ciudad values(null,'Mártir de Cuilapan','GRO');
insert into ciudad values(null,'Metlatónoc','GRO');
insert into ciudad values(null,'Mochitlán','GRO');
insert into ciudad values(null,'Olinalá','GRO');
insert into ciudad values(null,'Ometepec','GRO');
insert into ciudad values(null,'Pedro Ascencio Alquisiras','GRO');
insert into ciudad values(null,'Petatlán','GRO');
insert into ciudad values(null,'Pilcaya','GRO');
insert into ciudad values(null,'Pungarabato','GRO');
insert into ciudad values(null,'Quechultenango','GRO');
insert into ciudad values(null,'San Luis Acatlán','GRO');
insert into ciudad values(null,'San Marcos','GRO');
insert into ciudad values(null,'San Miguel Totolapan','GRO');
insert into ciudad values(null,'Taxco de Alarcón','GRO');
insert into ciudad values(null,'Tecoanapa','GRO');
insert into ciudad values(null,'Técpan de Galeana','GRO');
insert into ciudad values(null,'Teloloapan','GRO');
insert into ciudad values(null,'Tepecoacuilco de Trujano','GRO');
insert into ciudad values(null,'Tetipac','GRO');
insert into ciudad values(null,'Tixtla de Guerrero','GRO');
insert into ciudad values(null,'Tlacoachistlahuaca','GRO');
insert into ciudad values(null,'Tlacoapa','GRO');
insert into ciudad values(null,'Tlalchapa','GRO');
insert into ciudad values(null,'Tlalixtlaquilla de Maldanado','GRO');
insert into ciudad values(null,'Tlapa de Comonfort','GRO');
insert into ciudad values(null,'Tlapehuala','GRO');
insert into ciudad values(null,'Xalpatláhuac','GRO');
insert into ciudad values(null,'Xochihuehuetlán','GRO');
insert into ciudad values(null,'Xochistlahuaca','GRO');
insert into ciudad values(null,'Zapotitlán Tablas','GRO');
insert into ciudad values(null,'Zihuatanejo de Azueta','GRO');
insert into ciudad values(null,'Zirándaro de los Chávez','GRO');
insert into ciudad values(null,'Zitlala','GRO');

insert into ciudad values(null,'Acatlán','HID');
insert into ciudad values(null,'Acaxochitlán','HID');
insert into ciudad values(null,'Actopan','HID');
insert into ciudad values(null,'Agua Blanca de Iturbide','HID');
insert into ciudad values(null,'Ajacuba','HID');
insert into ciudad values(null,'Alfajayucan','HID');
insert into ciudad values(null,'Almoloya','HID');
insert into ciudad values(null,'Apan','HID');
insert into ciudad values(null,'Atitalaquia','HID');
insert into ciudad values(null,'Atlapexco','HID');
insert into ciudad values(null,'Atotonilco de Tula','HID');
insert into ciudad values(null,'Atotonilco el Grande','HID');
insert into ciudad values(null,'Calnali','HID');
insert into ciudad values(null,'Chapantongo','HID');
insert into ciudad values(null,'Chapulhuacán','HID');
insert into ciudad values(null,'Cardonal','HID');
insert into ciudad values(null,'Chilcuautla','HID');
insert into ciudad values(null,'Cuautepec de Hinojosa','HID');
insert into ciudad values(null,'El Arenal','HID');
insert into ciudad values(null,'Eloxochitlán','HID');
insert into ciudad values(null,'Emiliano Zapata','HID');
insert into ciudad values(null,'Epazoyucan','HID');
insert into ciudad values(null,'Francisco I. Madero','HID');
insert into ciudad values(null,'Huasca de Ocampo','HID');
insert into ciudad values(null,'Huautla','HID');
insert into ciudad values(null,'Huazalingo','HID');
insert into ciudad values(null,'Huejutla de Reyes','HID');
insert into ciudad values(null,'Huehuetla  ','HID');
insert into ciudad values(null,'Huichapan','HID');
insert into ciudad values(null,'Ixmiquilpan','HID');
insert into ciudad values(null,'Jacala de Ledezma','HID');
insert into ciudad values(null,'Jaltocán','HID');
insert into ciudad values(null,'Juárez Hidalgo','HID');
insert into ciudad values(null,'La Misión','HID');
insert into ciudad values(null,'Lolotla','HID');
insert into ciudad values(null,'Metepec','HID');
insert into ciudad values(null,'Metztitlán','HID');
insert into ciudad values(null,'Mineral de la Reforma','HID');
insert into ciudad values(null,'Mineral del Chico','HID');
insert into ciudad values(null,'Mineral del Monte','HID');
insert into ciudad values(null,'Mixquiahuala de Juárez','HID');
insert into ciudad values(null,'Molango de Escamilla','HID');
insert into ciudad values(null,'Nicolás Flores','HID');
insert into ciudad values(null,'Nopala de Villagrán','HID');
insert into ciudad values(null,'Omitlán de Juárez','HID');
insert into ciudad values(null,'Pachuca de Soto','HID');
insert into ciudad values(null,'Pacula','HID');
insert into ciudad values(null,'Pisaflores','HID');
insert into ciudad values(null,'Progreso de Obregón','HID');
insert into ciudad values(null,'San Agustín Metzquititlán','HID');
insert into ciudad values(null,'San Agustín Tlaxiaca','HID');
insert into ciudad values(null,'San Bartolo Tutotepec','HID');
insert into ciudad values(null,'San Felipe Orizatlán','HID');
insert into ciudad values(null,'San Salvador','HID');
insert into ciudad values(null,'Santiago de Anaya','HID');
insert into ciudad values(null,'Santiago Tulantepec de Lugo Guerrero','HID');
insert into ciudad values(null,'Singuilucan','HID');
insert into ciudad values(null,'Tasquillo','HID');
insert into ciudad values(null,'Tecozautla','HID');
insert into ciudad values(null,'Tenango de Doria','HID');
insert into ciudad values(null,'Tepeapulco','HID');
insert into ciudad values(null,'Tepehuacán de Guerrero','HID');
insert into ciudad values(null,'Tepeji del Río de Ocampo','HID');
insert into ciudad values(null,'Tepetitlán','HID');
insert into ciudad values(null,'Tetepango','HID');
insert into ciudad values(null,'Tezontepec de Aldama','HID');
insert into ciudad values(null,'Tianguistengo','HID');
insert into ciudad values(null,'Tizayuca','HID');
insert into ciudad values(null,'Tlahuelilpan','HID');
insert into ciudad values(null,'Tlahuiltepa','HID');
insert into ciudad values(null,'Tlanalapa','HID');
insert into ciudad values(null,'Tlanchinol','HID');
insert into ciudad values(null,'Tlaxcoapan','HID');
insert into ciudad values(null,'Tolcayuca','HID');
insert into ciudad values(null,'Tula de Allende','HID');
insert into ciudad values(null,'Tulancingo de Bravo','HID');
insert into ciudad values(null,'Villa de Tezontepec','HID');
insert into ciudad values(null,'Xochiatipan','HID');
insert into ciudad values(null,'Xochicoatlán','HID');
insert into ciudad values(null,'Yahualica','HID');
insert into ciudad values(null,'Zacualtipán de Ángeles','HID');
insert into ciudad values(null,'Zapotlán de Juárez','HID');
insert into ciudad values(null,'Zempoala','HID');
insert into ciudad values(null,'Zimapán','HID');

select * from ciudad;

drop table profesor;
create table profesor(idprofesor int primary key auto_increment,
nocontrolp varchar(50),
nombre varchar(50), 
apellidopaterno varchar(50), 
apellidomaterno varchar(50),
Direccion varchar(50),
Ciudad varchar(50),
Estado varchar(50),
numerocedula varchar(50),
titulo varchar(50),
fechanacimiento varchar(50));

select max(SUBSTRING(nocontrolp,6)) as nocontrol from profesor where nocontrolp like '%2019%';

select * from profesor;

Insert into profesor values(null,'D2019','Rafael','Gonzalez','Romero','Arboledas #32','Jalisco','lagos de Moreno','SGUE15','Doctorado','1999/05/29');

drop table estudios;
create table estudios(idestudio int primary key auto_increment,
estudio varchar(50),
fkidprofesor int,
documento varchar(300),
foreign key (fkidprofesor) references profesor (idprofesor));



insert into estudios values(null,'Bachillerato','2','Act1');
select * from estudios;

select idestudio, nombre, estudio, documento from estudios,profesor where estudios.fkidprofesor = profesor.idprofesor;

drop table materia;
create table materia(idmateria int primary key auto_increment, 
nombremateria varchar(50), 
hrsteoria int, 
hrspractica int,
creditos int);

insert into materia values(null,'Fundamentos de Base de Datos',3,2,5);
insert into materia values(null,'Taller de Base de Datos',3,3,6);
insert into materia values(null,'Administracion de Base de Datos',3,2,5);

insert into materia values(null,'Contabilidad',4,1,5);

select * from materia;

drop table materiarelacion;
create table materiarelacion(idmateriarelacion int primary key auto_increment,
materia varchar(100),
antes varchar(100),
despues varchar(100));


insert into materiarelacion values(null,2,1,3);
select * from materiarelacion;


select idmateriarelacion, nombremateria, nombremateria, nombremateria from materia,materiarelacion
where materiarelacion.fkmateria = materia.idmateria or materiarelacion.fkantes = materia.idmateria;



select idmateriarelacion as 'ID', fkmateria as 'Materia',hrsteoria as 'Horas Teoria', hrspractica as 'Horas Practica',
creditos as 'Creditos', fkantes as 'Materia Anterior',creditos as 'Creditos', fkdespues as 'Materia Sucesora'
from materiarelacion,materia where materiarelacion.fkantes = materia.idmateria;

drop table escuela;
create table escuela(idescuela int primary key auto_increment,
nombre varchar(100),
RFC varchar(50),
domicilio varchar(50),
telefono varchar(20),
correo varchar(50),
paginaweb varchar(200),
director varchar(50),
logo varchar(50));

select * from escuela;

insert into escuela values(null,'Jose Mario Molina Pasquel y Henriquez','MM123644','Libramiento Tecnológico No. 5000','474-741-2474','tecmmlago@gmail.com','www.lagos.tecmm.edu.mx','Maria','hwu');

