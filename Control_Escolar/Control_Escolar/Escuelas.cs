﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;
using System.Data.SqlClient;


namespace Control_Escolar
{
    public partial class Escuelas : Form
    {
        private EscuelaManejador _escuelaManejador;
        private OpenFileDialog _imagenJpg;
        private string _rutas;
        //private Escuela _escuela;

        public Escuelas()
        {
            InitializeComponent();
            _escuelaManejador = new EscuelaManejador();
            _imagenJpg = new OpenFileDialog();
            _rutas = Application.StartupPath + "\\Logo\\";
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void Escuela_Load(object sender, EventArgs e)
        {
            Registros();
            ControlarCuadros(false);
            ControlarBotones(false, true, true, false);
            ControlarBotones(false, false, false, true);
            
        }

        private DataSet Registros()
        {
            DataSet ds;
            ds = _escuelaManejador.Ver();
            {
                txtId.Text = ds.Tables[0].Rows[0]["idescuela"].ToString();
                txtNombre.Text = ds.Tables[0].Rows[0]["nombre"].ToString();
                txtDirector.Text = ds.Tables[0].Rows[0]["director"].ToString();
                txtRFC.Text = ds.Tables[0].Rows[0]["RFC"].ToString();
                txtDomicilio.Text = ds.Tables[0].Rows[0]["domicilio"].ToString();
                txtEmail.Text = ds.Tables[0].Rows[0]["correo"].ToString();
                mtxtTelefono.Text = ds.Tables[0].Rows[0]["telefono"].ToString();
                txtPaginaWeb.Text = ds.Tables[0].Rows[0]["paginaweb"].ToString();
                txtNombreImagen.Text = ds.Tables[0].Rows[0]["logo"].ToString();
                txtLogo.ImageLocation = _rutas + "\\" + ds.Tables[0].Rows[0]["logo"].ToString();

            }
            return ds;
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtId.Enabled = activar;
            txtNombre.Enabled = activar;
            txtRFC.Enabled = activar;
            txtDomicilio.Enabled = activar;
            mtxtTelefono.Enabled = activar;
            txtEmail.Enabled = activar;
            txtPaginaWeb.Enabled = activar;
            txtDirector.Enabled = activar;
            txtNombreImagen.Enabled = activar;
            txtLogo.Enabled = activar;
            btnCargarJpg.Enabled = activar;
            btnEliminarJpj.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtRFC.Text = "";
            txtDomicilio.Text = "";
            mtxtTelefono.Text = "";
            txtEmail.Text = "";
            txtPaginaWeb.Text = "";
            txtDirector.Text = "";
            txtLogo.Image = null;



        }
        private void GuardarEscuela()
        {
            if (string.IsNullOrEmpty(txtNombreImagen.Text))
            {
                MessageBox.Show("Es necesario el logo de la escuela");
            }
            else
            {

                _escuelaManejador.Guardar(new Escuela
                {
                    Idescuela = Convert.ToInt32(txtId.Text),
                    Nombre = txtNombre.Text,
                    RFC = txtRFC.Text,
                    Domicilio = txtDomicilio.Text,
                    Telefono = mtxtTelefono.Text,
                    Correo = txtEmail.Text,
                    Paginaweb = txtPaginaWeb.Text,
                    Director = txtDirector.Text,
                    Logo = txtNombreImagen.Text,

                });
            }
        }
        private void EliminarEscuela()
        {
            var Idescuela = dtgEscuela.CurrentRow.Cells["idescuela"].Value;
            _escuelaManejador.Eliminar(Convert.ToInt32(Idescuela));


        }
        private void ModificarEscuela()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            /*lblId.Text = dtgEscuela.CurrentRow.Cells["idescuela"].Value.ToString();
            txtNombre.Text = dtgEscuela.CurrentRow.Cells["nombre"].Value.ToString();
            txtRFC.Text = dtgEscuela.CurrentRow.Cells["RFC"].Value.ToString();
            txtDomicilio.Text = dtgEscuela.CurrentRow.Cells["domicilio"].Value.ToString();
            mtxtTelefono.Text = dtgEscuela.CurrentRow.Cells["telefono"].Value.ToString();
            txtEmail.Text = dtgEscuela.CurrentRow.Cells["correo"].Value.ToString();
            txtPaginaWeb.Text = dtgEscuela.CurrentRow.Cells["paginaweb"].Value.ToString();
            txtDirector.Text = dtgEscuela.CurrentRow.Cells["director"].Value.ToString();
            txtNombreImagen.Text = dtgEscuela.CurrentRow.Cells["logo"].Value.ToString();

            txtLogo.ImageLocation = _rutas + "\\" + dtgEscuela.CurrentRow.Cells["logo"].Value.ToString();
            
            */
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            //buscarescuela(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (dtgEscuela.RowCount - 1 < 0)
            {
                ControlarBotones(false, true, true, false);
                ControlarCuadros(true);
                txtNombre.Focus();


            }
            else
            {
                MessageBox.Show("NO PUEDES INGRESAR MAS DE UN REGISTRO");

            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
                ControlarBotones(true, false, false, true);
                ControlarCuadros(false);

            if (txtLogo.Image == null)
            {
                EliminarImagenJpg();
            }

            else 
            {


                try
                {
                    GuardarEscuela();
                    GuardarImagenJpg();

                    btnNuevo.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }

            }
                //LimpiarCuadros();
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Registros();
            ControlarBotones(false, true, true, true);
            ControlarCuadros(true);

            //LimpiarCuadros();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarEscuela();
                    EliminarImagenJpg();
                    //buscarescuela("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgEscuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {

                ModificarEscuela();
                //buscarescuela("");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtDirector_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCargarJpg_Click(object sender, EventArgs e)
        {
            CargarImagenJpg();
        }
        private void CargarImagenJpg()
        {
            _imagenJpg.Filter = "Imagen tipo (*.jpg)|*.jpg";
            _imagenJpg.Title = "Cargar Imagen";
            _imagenJpg.ShowDialog();

            if (_imagenJpg.FileName != "")
            {

                var archivo = _imagenJpg.FileName;
                Bitmap picture = new Bitmap(archivo);
                var archivos = new FileInfo(_imagenJpg.FileName);
                if (archivos.Length < 3000000)
                {
                    txtLogo.Image = picture;
                    txtNombreImagen.Text = archivos.Name;

                }
                else
                {
                    MessageBox.Show("No se pueden cargar imagenes mayores a 5mb");
                }



            }
        }
        private void GuardarImagenJpg()
        {
            if (_imagenJpg.FileName != null)
            {
                if (_imagenJpg.FileName != "")
                {
                    var archivos = new FileInfo(_imagenJpg.FileName);

                    if (Directory.Exists(_rutas))
                    {
                        //codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_rutas, "*.jpg");
                        FileInfo archivoAnterior;


                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivos.CopyTo(_rutas + archivos.Name);
                            }
                        }
                        else
                        {
                            archivos.CopyTo(_rutas + archivos.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_rutas);
                        archivos.CopyTo(_rutas + archivos.Name);
                    }
                }

            }
        }
        private void EliminarImagenJpg()
        {
               if (Directory.Exists(_rutas))
                {
                    //codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_rutas, "*.jpg");
                    FileInfo archivoAnterior;


                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }
                    }

                }

            

        }

        private void btnEliminarPDF_Click(object sender, EventArgs e)
        {
            
            //EliminarImagenJpg();
            txtLogo.Image = null;
            txtLogo.Enabled = false;
            txtNombreImagen.Text = " ";
           


        }

        private void txtRFC_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnModifica_Click(object sender, EventArgs e)
        {
            ModificarEscuela();
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            txtId.Enabled = false;

        }
        private void CargarEscuela()
        {
            /*_escuela.Idescuela = Convert.ToInt32(txtId.Text);
            _escuela.Nombre = txtNombre.Text;
            _escuela.RFC = txtRFC.Text;
            _escuela.Domicilio = txtDomicilio.Text;
            _escuela.Telefono = txtDomicilio.Text;
            _escuela.Correo = txtEmail.Text;
            _escuela.Paginaweb = txtPaginaWeb.Text;
            _escuela.Director = txtDirector.Text;
            _escuela.Logo = txtNombreImagen.Text;
            */
        }

    }
}
