﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace Control_Escolar
{
    public partial class RelacionMateria : Form
    {
        private MateriaManejador _materiaManejador;
        private RelacionMateriaManejador _relacionMateriaManejador;

        public RelacionMateria()
        {
            InitializeComponent();
            _materiaManejador = new MateriaManejador();
            _relacionMateriaManejador = new RelacionMateriaManejador();
        }

        private void RelacionMateria_Load(object sender, EventArgs e)
        {
            buscarRelacionMateria("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnGuardarMateria_Click(object sender, EventArgs e)
        {         
        }
        private void btnMaterias_Click(object sender, EventArgs e)
        {
            Materia materia = new Materia();
            materia.Show();
        }
        private void buscarRelacionMateria(string filtro)
        {

            dtgRelacionMateria.DataSource = _relacionMateriaManejador.GetRelacionMaterias(filtro);
        }
        private void TraerMateria(string filtro)
        {
            cmbMateria.DataSource = _materiaManejador.GetMateria(filtro);
            cmbMateria.DisplayMember = "nombremateria";
            cmbMateria.ValueMember = "idmateria";

        }
        private void TraerMateriaA(string filtro)
        {
            cmbAntecesora.DataSource = _materiaManejador.GetMateria(filtro);
            cmbAntecesora.DisplayMember = "nombremateria";
            cmbAntecesora.ValueMember = "idmateria";

        }
        private void TraerMateriaD(string filtro)
        {
            cmbSucesora.DataSource = _materiaManejador.GetMateria(filtro);
            cmbSucesora.DisplayMember = "nombremateria";
            cmbSucesora.ValueMember = "idmateria";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {           
            cmbMateria.Enabled = activar;
            cmbAntecesora.Enabled = activar;
            cmbSucesora.Enabled = activar;
           
        }
        private void LimpiarCuadros()
        {
            cmbMateria.Text = "";
            cmbAntecesora.Text = "";
            cmbSucesora.Text = "";
            
        }
        private void GuardarRelacionMateria()
        {
            _relacionMateriaManejador.Guardar(new RelacionMaterias
            {
                Idrelacion = Convert.ToInt32(lblId.Text),
                Materia = cmbMateria.Text,              
                Antesesora = cmbAntecesora.Text,
                Sucesora= cmbSucesora.Text,

          
            });
        }
        private void EliminarRelacionMateria()
        {
            var Idrelacion = dtgRelacionMateria.CurrentRow.Cells["idrelacion"].Value;
            _relacionMateriaManejador.Eliminar(Convert.ToInt32(Idrelacion));
        }
        private void ModificarRelacionMateria()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgRelacionMateria.CurrentRow.Cells["idrelacion"].Value.ToString();
            cmbMateria.Text = dtgRelacionMateria.CurrentRow.Cells["materia"].Value.ToString();
            cmbAntecesora.Text = dtgRelacionMateria.CurrentRow.Cells["antesesora"].Value.ToString();
            cmbSucesora.Text = dtgRelacionMateria.CurrentRow.Cells["sucesora"].Value.ToString();
            

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarRelacionMateria(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbMateria.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarRelacionMateria();                

                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarRelacionMateria();
                    buscarRelacionMateria("");
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgRelacionMateria_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarRelacionMateria();
                buscarRelacionMateria("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cmbAntecesora_Click(object sender, EventArgs e)
        {
            TraerMateriaA("");
        }

        private void cmbSucesora_Click(object sender, EventArgs e)
        {
            TraerMateriaD("");
        }
        private void cmbAntecesora_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void cmbSucesora_SelectedIndexChanged(object sender, EventArgs e)
        {         
        }
        private void txtCreditos_TextChanged(object sender, EventArgs e)
        {           
        }
        private void txtCreditos_Leave(object sender, EventArgs e)
        {         
        }
        private void txtHrsPractica_Leave(object sender, EventArgs e)
        {          
        }

        private void txtHrsPractica_Enter(object sender, EventArgs e)
        {      
        }
        private void button1_Click(object sender, EventArgs e)
        {           
        }
        private void cmbMateria_Click(object sender, EventArgs e)
        {
            TraerMateria("");
        }
    }
}
