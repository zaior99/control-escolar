﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _usuarioAccesoDatos = new UsuarioAccesoDatos();

        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");//VALIDAR @ Delimitador, ^ Inicio con una letra [A-Z] (mayuscula) y despues concatene (espacio) + letra [A-Z] (mayuscula),$ Fin
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool ApellidoPaternoValido(string apellidopaterno)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(apellidopaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool ApellidoMaternoValido(string apellidomaterno)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(apellidomaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        //tuple retorna el numero de variables de diferente tipo de dato
        public Tuple<bool,string> ValidarUsuario(Usuario usuario)
        {
            string mensaje = "";
            bool valido = true;
            //Nombre
            if (usuario.Nombre.Length == 0)
            {
                mensaje = mensaje + "El nombre de Usuario es necesario \n";
                valido = valido = false;
            }
            else if (usuario.Nombre.Length > 20)
            {
                mensaje = mensaje + "El nombre de Usuario solo permite un maximo de 20 carecteres \n";
                valido = valido = false;
            }
            else if (!NombreValido(usuario.Nombre))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre del usuario \n";
                valido = false;
            }
            //Apellido Paterno
            if (usuario.Apellidopaterno.Length == 0)
            {
                mensaje = mensaje + "El apellido paterno de Usuario es necesario \n";
                valido = valido = false;
            }
            else if (usuario.Apellidopaterno.Length > 20)
            {
                mensaje = mensaje + "El apellido paterno de Usuario solo permite un maximo de 20 carecteres \n";
                valido = valido = false;
            }
            else if (!ApellidoPaternoValido(usuario.Apellidopaterno))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre del usuario \n";
                valido = false;
            }
            //Apellido Materno
            if (usuario.Apellidomaterno.Length == 0)
            {
                mensaje = mensaje + "El apellido paterno de Usuario es necesario \n";
                valido = valido = false;
            }
            else if (usuario.Apellidomaterno.Length > 20)
            {
                mensaje = mensaje + "El apellido paterno de Usuario solo permite un maximo de 20 carecteres \n";
                valido = valido = false;
            }
            else if (!ApellidoPaternoValido(usuario.Apellidomaterno))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre del usuario \n";
                valido = false;
            }


            return Tuple.Create(valido, mensaje);
        }
        public void Guardar(Usuario usuario)
        {
            _usuarioAccesoDatos.Guardar(usuario);

        }
        public void Eliminar(int idUsuario)
        {
            _usuarioAccesoDatos.Eliminar(idUsuario);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            var listUsuario = _usuarioAccesoDatos.GetUsuarios(filtro);

            return listUsuario;
        }
    }
}
