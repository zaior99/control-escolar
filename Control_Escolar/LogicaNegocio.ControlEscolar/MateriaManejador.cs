﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class MateriaManejador
    {
        private MateriaAccesoDatos _materiaAccesoDatos = new MateriaAccesoDatos();

        public void Guardar(Materias materias)
        {
            _materiaAccesoDatos.Guardar(materias);

        }
        public void Eliminar(int idMateria)
        {
            _materiaAccesoDatos.Eliminar(idMateria);
        }
        public List<Materias> GetMateria(string filtro)
        {
            var listMateria = _materiaAccesoDatos.GetMateria(filtro);

            return listMateria;
        }
    }
}
