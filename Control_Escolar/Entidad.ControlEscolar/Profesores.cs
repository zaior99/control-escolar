﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Profesores
    {
        private int _idprofesor;
        private string _nocontrolp;
        private string _nombre;
        private string _apellidopaterno;
        private string _apellidomaterno;
        private string _direccion;
        private string _ciudad;
        private string _estado;
        private string _numerocedula;
        private string _titulo;
        private string _fechanacimeinto;

        public int Idprofesor { get => _idprofesor; set => _idprofesor = value; }
        public string Nocontrolp { get => _nocontrolp; set => _nocontrolp = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string Apellidomaterno { get => _apellidomaterno; set => _apellidomaterno = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Numerocedula { get => _numerocedula; set => _numerocedula = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Fechanacimeinto { get => _fechanacimeinto; set => _fechanacimeinto = value; }
    }
}
